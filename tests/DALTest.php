<?php
/**
 * File: tests/DALTest.php
 *
 * @author Robert Polsen
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.1
 *
 */

// NS
namespace DataAccessDAL\tests;

// Use
use DataAccessDAL\DAL as DAL;
use Core\Mongo\IndexManipulator as IndexManipulator;
use Dotenv\Dotenv as Dotenv;

/**
 * File: DALTest.php
 *
 * @author victor, Francesc Roma Frigole cesc@roma.cat, mrx
 * @version 1.0
 * @package DataAccessDAL
 * @subpackage tests
 */
class DALTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test type: acceptance|unit
     * @var String
     */
    private $TEST_TYPE;

    /**
     * Data access Layer
     * @var object
     */
    private $dal;
    
    /**
     * Just check test DB is there.
     * Functional/Acceptance tests need some real data.
     * (not really, every time we do a test everything resets.)
     * It depends on .env
     *
     */ 
    public function setUp()
    {

        // Load Configuration
        $dotenv = new Dotenv(dirname(__DIR__));
        $dotenv->load();
        // TEST_TYPE
        $this->TEST_TYPE = getenv('TEST_TYPE');

        // make sure that the collection SA24 Catalogue exists in the db
        $this->dal = DAL::getInstance();
        switch ($this->TEST_TYPE) {
            case 'unit':
                $this->dal->setCollections("GS1_catalogue_test_unit", "SA24_catalogue_test_unit");
                break;
            case 'acceptance':
                $this->dal->setCollections("GS1_catalogue_test_acceptance", "SA24_catalogue_test_acceptance");
            default:
                
        }
        
        $collections = $this->dal->getCollectionNames();
        $collections = array_map(
            function ($v) {
                return $v->name;
            },
            $collections
        );
        
        switch ($this->TEST_TYPE) {
            case 'unit':
                if (in_array("GS1_catalogue_test_unit", $collections)) {
                    $this->dal->dropCollection("GS1_catalogue_test_unit");
                }
                $this->dal->createCollection("SA24_catalogue_test_unit");
                $this->dal->createCollection("GS1_catalogue_test_unit");
                $idx = new IndexManipulator();
                $idx->createIndexes([
                    'mongoServer' => 'localhost',
                    'mongoPort' => 27017,
                    'mongoDBName' => 'SA24',
                    'mongoCollectionName' => 'GS1_catalogue_test_unit',
                    'mongoCollectionSA24Name' => 'SA24_catalogue_test_unit',
                ]);
                break;
            case 'acceptance':
                break;
            default:
                # code...
        }
    }

    /**
     * Dropping Test Db depending on .env
     *
     */
    public function tearDown()
    {
        switch ($this->TEST_TYPE) {
            case 'unit':
                $mongoServer = 'localhost';
                $mongoPort = 27017;
                $manager = new \MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");

                $command = new \MongoDB\Driver\Command([
                    'drop' => 'SA24_catalogue_test_unit',
                ]);
                $manager->executeCommand('SA24', $command);

                $command = new \MongoDB\Driver\Command([
                    'drop' => 'GS1_catalogue_test_unit',
                ]);
                $manager->executeCommand('SA24', $command);
                break;
            case 'test':
                # code...
                break;
            default:
                # code...
        }
    }


    public function testCollectionsExist()
    {
        $dal = DAL::getInstance();
        $collections = $dal->getCollectionNames();
        $collections = array_map(
            function ($v) {
                return $v->name;
            },
            $collections
        );
        switch ($this->TEST_TYPE) {
            case 'unit':
                $this->assertTrue(in_array("SA24_catalogue_test_unit", $collections));
                $this->assertTrue(in_array("GS1_catalogue_test_unit", $collections));
                break;
            case 'acceptance':
                $this->assertTrue(in_array("SA24_catalogue_test_acceptance", $collections));
                $this->assertTrue(in_array("GS1_catalogue_test_acceptance", $collections));
                break;
            default:
                # code...
        }   
    }

    /*
    public function testGenerateNewTokenData()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com',
            'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
            'ACLRole'=>'Supplier_Editor',
            'glns'=> ['5790000010974']
        ];
        $dal = DAL::getInstance();
        $result = $dal->personTokenCreate((object)$person);
        $this->assertTrue(!!$result);
    }

    private function populatePersonDB()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com',
            'password'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
            'ACLRole'=>'Supplier_Editor',
            'glns'=> ['5790000010974']
        ];
        $dal = DAL::getInstance();
        $dal->personSave((object)$person);
        $person =[
            'client'=>'shobrManager',
            'email'=> rand(0, 5000) . '@' . rand(0, 5000) . '.org',
            'password'=> '123',
            'ACLRole'=> 'Supplier_Editor',
            'glns'=> '11110000010974'
        ];
        $dal = DAL::getInstance();
        return $dal->personSave((object)$person);
    }


    /** 
     * @todo without filters and in a foreach(person) loop 
     */
    /*
    public function testPersonRead()
    {
        $this->populatePersonDB();
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com'
        ];
        $dal = DAL::getInstance();
        
        $result = $dal->personRead(['filters' => $person]);
        $this->assertTrue(is_array($result));

        $requireds = ['client','email','password','ACLRole','glns'];
        $r = (array)$result[0];
        $test = true ;
        foreach ($requireds as $required) {
            if (!array_key_exists($required, $r)) {
                $test = false ;
            }
        }
        $this->assertTrue($test);
    }


    public function testPersonSave()
    {
        $result = $this->populatePersonDB();
        $this->assertTrue(1 == $result->getUpsertedCount()|| 1 == $result->getInsertedCount());
    }

    public function testTokenCreate()
    {
        $dal = DAL::getInstance();
        $person = [
            'email' => 'eko@shopall24.com'
        ];
        $person = $dal->personRead(['filters' => $person]);
        $token = $dal->tokenCreate($person[0]);
        $this->assertTrue(is_object($token) && isset($token->code) && isset($token->expire) && isset($token->person_id));
        $dal->tokenDelete(["filters"=>(array)$token]);
    }

    public function testTokenSave()
    {
        $dal = DAL::getInstance();
        $token = [];
        $token["code"] = "698cd78f0c5bed6bcd6871c43602cff5c2c5ab400e188362f518b5492660ef0ebc40018d7aa718f54f7f796efc2ec0d6b1ea73c1691492bd1c9e0b790d9192ab50d8ef8f5762497042f3048cde8d";
        $token["expired"] = "2016-08-18T20:11:25+02:00";

        $result = $dal->tokenSave((object)$token);
        $this->assertTrue(1 == $result->getUpsertedCount() || 1 == $result->getInsertedCount());
        $dal->tokenDelete(["filters"=>$token]);
    }

    public function testTokenRead()
    {
        $dal = DAL::getInstance();
        $person = [
            'email' => 'eko@shopall24.com'
        ];
        $person = $dal->personRead(['filters' => $person]);
        $token = $dal->tokenCreate($person[0]);
        $this->assertTrue(is_object($token) && isset($token->code) && isset($token->expire) && isset($token->person_id));
        $tokenRead = $dal->tokenRead(["filters"=>(array)$token]);
        $this->assertTrue(is_object($tokenRead));
        $diff = array_diff((array)$token, (array)$tokenRead);
        $this->assertTrue(count($diff) == 0);
        $dal->tokenDelete(["filters"=>(array)$token]);
    }

    public function testTokenDelete()
    {
        $dal = DAL::getInstance();
        $person = [
            'email' => 'eko@shopall24.com'
        ];
        $person = $dal->personRead(['filters' => $person]);
        $token = $dal->tokenCreate($person[0]);
        $this->assertTrue(is_object($token) && isset($token->code) && isset($token->expire) && isset($token->person_id));
        $result = $dal->tokenDelete(["filters"=>(array)$token]);
        $this->assertTrue(1 == $result->getDeletedCount());
    }


    public function testPersonTokenCreate()
    {
        $person =[
            'client'=>'shobrManager',
            'email'=>'eko@shopall24.com'
        ];
        $dal = DAL::getInstance();
        $dal->personTokenCreate((object) $person);
        $this->assertTrue(array_key_exists('code', (array)$dal->token));
        $this->assertTrue(array_key_exists('expire', (array)$dal->token));
    }
    */


    public function testCatalogueItemReadGlnWithoutFields()
    {
        $this->populateDB();
        $catalogue =[
            'tradeItem.informationProviderOfTradeItem.gln' => '1'
        ];
        $dal = DAL::getInstance();
        $result = $dal->catalogueItemRead(['filters' => $catalogue]);
        $this->assertTrue(is_array($result));
        $this->assertTrue(is_array($result['GS1']));
        $this->assertTrue(is_array($result['SA24']));
        $this->assertTrue(!empty($result['GS1']));

        foreach (array_merge($result['GS1'], $result['SA24']) as $catalogueItem) {
            $this->assertEquals(
                $catalogueItem->tradeItem->informationProviderOfTradeItem->gln,
                '1'
            );
        }
    }

    public function testCatalogueItemReadGlnWithFields()
    {
        $catalogue =[
            'tradeItem.informationProviderOfTradeItem.gln' => '4'
        ];
        $fields = [
             'catalogueItem.tradeItem.gtin',
             'catalogueItem.tradeItem.informationProviderOfTradeItem.gln',
        ];
        $dal = DAL::getInstance();
        $result = $dal->catalogueItemRead(['filters' => $catalogue, 'fields' => $fields]);
        $this->assertTrue(is_array($result));
        $this->assertTrue(is_array($result['GS1']));
        $this->assertTrue(is_array($result['SA24']));

        foreach (array_merge($result['GS1'], $result['SA24']) as $catalogueItem) {
            $this->assertEquals(
                $catalogueItem->tradeItem->informationProviderOfTradeItem->gln,
                '4'
            );
            // gtin, gln is in fields
            $this->assertObjectHasAttribute('gtin', $catalogueItem->tradeItem);
            $this->assertObjectHasAttribute('gln', $catalogueItem->tradeItem->informationProviderOfTradeItem);
            // isTradeItemADespatchUnit is not in fields
            $this->assertObjectNotHasAttribute(
                'isTradeItemADespatchUnit',
                $catalogueItem->tradeItem
            );
        }
    }

    public function testCatalogueItemReadGtin()
    {
        $this->populateDB();
        $catalogue =[
            'tradeItem.gtin'=>'3',
        ];
        $dal = DAL::getInstance();
        $result = $dal->catalogueItemRead(['filters' => $catalogue]);
        $this->assertTrue(is_array($result));

        $GS1_catalogueItems = $result['GS1'];
        $SA24_catalogueItems = $result['SA24'];

        $this->assertTrue(is_array($GS1_catalogueItems));
        $this->assertTrue(is_array($SA24_catalogueItems));

        $this->assertCount(1, $GS1_catalogueItems);
        $this->assertCount(1, $SA24_catalogueItems);

        $catalogueItem = array_pop($GS1_catalogueItems);
        $this->assertEquals($catalogueItem->tradeItem->gtin, '3');

        $catalogueItem = array_pop($SA24_catalogueItems);
        $this->assertEquals($catalogueItem->tradeItem->gtin, '3');
    }


    public function testCatalogueItemReadGtinAndGln()
    {
        $this->populateDB();
        $catalogue =[
            'tradeItem.gtin'=>'2',
            'tradeItem.informationProviderOfTradeItem.gln' => '2'
        ];
        $dal = DAL::getInstance();
        $result = $dal->catalogueItemRead(['filters' => $catalogue]);
        $this->assertTrue(is_array($result));

        $GS1_catalogueItems = $result['GS1'];
        $SA24_catalogueItems = $result['SA24'];

        $this->assertTrue(is_array($GS1_catalogueItems));
        $this->assertTrue(is_array($SA24_catalogueItems));

        foreach ($GS1_catalogueItems as $catalogueItem) {
            $this->assertEquals($catalogueItem->tradeItem->gtin, '2');
            $this->assertEquals(
                $catalogueItem->tradeItem->informationProviderOfTradeItem->gln,
                '2'
            );
        }

        $this->assertCount(1, $GS1_catalogueItems);
        $this->assertCount(1, $SA24_catalogueItems);
    }

    /**
     *
     */ 
    public function testCatalogueItemReadGtinNotFound()
    {
        $catalogue =[
            'catalogueItem.tradeItem.gtin'=>'not_a_valid_gtin_id'
        ];
        $dal = DAL::getInstance();
        $result = $dal->catalogueItemRead(['filters' => $catalogue]);
        $this->assertTrue(is_array($result));

        $GS1_catalogueItems = $result['GS1'];
        $SA24_catalogueItems = $result['SA24'];

        $this->assertTrue(is_array($GS1_catalogueItems));
        $this->assertTrue(is_array($SA24_catalogueItems));

        // check that we got zero results
        $this->assertCount(0, $GS1_catalogueItems);
        $this->assertCount(0, $SA24_catalogueItems);
    }

    /**
     * Missing GLN|CVRNumber MUST be set
     */
    public function testCatalogueItemSaveNotValidIdentificationSupplier()
    {
        // start writing a catalogue item in the databse and chhecking we can
        // read it by id and find exactly the same thing
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'gtin' => '424242',
            'tradeItemFoo' => 'tradeItem bar',
            ]
        ];

        $this->setExpectedException('Exception');
        $this->dal->catalogueItemSave($catalogueOriginal);
    }

    /**
     * Missing GLN|CVRNumber MUST be set
     */
    public function testCatalogueItemSaveNotValidIdentificationProduct()
    {
        // start writing a catalogue item in the databse and chhecking we can
        // read it by id and find exactly the same thing
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '1234',
                ],
                'tradeItemFoo' => 'tradeItem bar',
            ]
        ];

        $this->setExpectedException('Exception');
        $this->dal->catalogueItemSave($catalogueOriginal);
    }

    /**
     * save operation
     */
    public function testCatalogueItemSave()
    {
        // start writing a catalogue item in the database and checking we can
        // read it by id and find exactly the same thing
        $catalogueOriginal = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '1234',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];
        $this->dal->catalogueItemSave($catalogueOriginal);

        $result = $this->dal->catalogueItemRead([
            'filters' => ['tradeItem.gtin' => '424242']
        ]);

        $this->assertCount(1, $result['SA24']);


        // the json encode/decode combination pattern to convert stdClass
        // to array is explained here
        // http://stackoverflow.com/questions/18576762/php-stdclass-to-array
        unset($result['SA24'][0]->_id);

        $this->assertEquals(
            json_decode(json_encode($result['SA24'][0]), true),
            (array) $catalogueOriginal
        );


        // now we modify the catalogue item and we check that we can update it
        // and read the updated record

        $catalogueModified = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'informationProviderOfTradeItem' => [
                    'gln' => '1234',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '424242',
            ]
        ];

        $result = $this->dal->catalogueItemSave($catalogueModified);

        $result = $this->dal->catalogueItemRead([
            'filters' => ['tradeItem.gtin' => '424242']
        ]);
        $this->assertCount(1, $result['SA24']);

        $expected = (array) json_decode(json_encode($result['SA24'][0]), true);

        $this->assertEquals(
            (array) $catalogueModified,
            $expected
        );
    }

    public function testCatalogueItemDelete()
    {
        $this->testCatalogueItemSave();
    // first check it exists
        $result = $this->dal->catalogueItemRead([
          'filters' => [
            'tradeItem.gtin' => '424242'
          ]
        ]);

        $this->assertCount(1, $result['SA24']);

        // then delete it
        $this->dal->catalogueItemDelete('424242');

        // now check is not there anymore
        $result = $this->dal->catalogueItemRead([
          'filters' => [
            'tradeItem.gtin' => '424242'
          ]
        ]);

        $this->assertCount(0, $result['SA24']);
    }
    /**
     * save operation
     */
    public function testCatalogueItemSaveBulk()
    {
        // start writing a catalogue item in the database and checking we can
        // read it by id and find exactly the same thing
        $catalogueList = [];
        for ($i = 0; $i < 10; $i++) {
            $catalogueList[]= json_decode(json_encode([
                'catalogue_foo' => 'catalogue bar',
                'tradeItem' => [
                    'informationProviderOfTradeItem' => [
                        'gln' => "1234$i",
                    ],
                    'tradeItemFoo' => 'tradeItem bar',
                    'gtin' => "424242$i",
                ]
            ]), false);
        }
        $this->dal->catalogueItemSaveBulk($catalogueList, 'SA24_test');

        $result = $this->dal->catalogueItemRead([
            'filters' => []
        ]);

        $this->assertCount(10, $result['SA24']);


        // the json encode/decode combination pattern to convert stdClass
        // to array is explained here
        // http://stackoverflow.com/questions/18576762/php-stdclass-to-array
        unset($result['SA24'][0]->_id);

        $this->assertEquals(
            json_decode(json_encode($result['SA24'][0]), true),
            json_decode(json_encode($catalogueList[9]), true)
        );


        // now we modify the catalogue item and we check that we can update it
        // and read the updated record
        $catalogueModified = [];
        for ($i = 0; $i < 10; $i++) {
            $catalogueModified[] = json_decode(json_encode([
                'catalogue_foo' => 'catalogue bar',
                'tradeItem' => [
                    'informationProviderOfTradeItem' => [
                        'gln' => "1234$i",
                    ],
                    'tradeItemFoo' => 'tradeItem bar',
                    'gtin' => "424242$i",
                ]
            ]), false);
        }
        $result = $this->dal->catalogueItemSaveBulk($catalogueModified, 'SA24_test');

        $result = $this->dal->catalogueItemRead([
            'filters' => [],
        ]);
        $this->assertCount(10, $result['SA24']);

        $expected = json_decode(json_encode($result['SA24'][0]), true);

        $this->assertEquals(
            json_decode(json_encode($catalogueModified[9]), true),
            $expected
        );
    }

    private function populateDB()
    {
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 42,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '1',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '1',
                'ean' => '1',
            ]
        ];
        $bulk[] = ['catalogueItem' => (array)$catalogueItem];
        $this->dal->catalogueItemSave($catalogueItem);
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 64,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '2',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '2',
                'ean' => '2',
            ]
        ];
        $bulk[] = ['catalogueItem' => (array)$catalogueItem];
        $this->dal->catalogueItemSave($catalogueItem);
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 231,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '3',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '3',
                'ean' => '3',
            ]
        ];
        $bulk[] = ['catalogueItem' => (array)$catalogueItem];
        $this->dal->catalogueItemSave($catalogueItem);
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 452,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '3',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '4',
                'ean' => '4',
            ]
        ];
        $bulk[] = ['catalogueItem' => (array)$catalogueItem];
        $this->dal->catalogueItemSave($catalogueItem);
        $this->dal->gs1ItemSaveBulk($bulk);
    }

    // public function testCatalogueItemSaveWithDuplicatedArticleNumber()
    // {
    //     $this->populateDB();
    //     $catalogueItem = (object) [
    //         'catalogue_foo' => 'catalogue bar',
    //         'tradeItem' => [
    //             'additionalTradeItemIdentification' => [
    //                 '@value' => 42,
    //             ],
    //             'informationProviderOfTradeItem' => [
    //                 'gln' => '3',
    //             ],
    //             'tradeItemFoo' => 'tradeItem bar',
    //             'gtin' => '1324',
    //             'ean' => '124',
    //         ]
    //     ];

    //     $this->setExpectedException('Exception');
    //     var_dump($this->dal->catalogueItemSave($catalogueItem));
    // }

    public function testCatalogueItemSaveWithDuplicatedEan()
    {
        $this->populateDB();
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 127,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '3',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '3',
                'ean' => '2',
            ]
        ];

        $this->setExpectedException('Exception');
        $this->dal->catalogueItemSave($catalogueItem, 'SA24_test');
    }

    public function testCatalogueItemSaveWithDuplicatedGtin()
    {
        $this->populateDB();
        $catalogueItem = (object) [
            'catalogue_foo' => 'catalogue bar',
            'tradeItem' => [
                'additionalTradeItemIdentification' => [
                    '@value' => 127,
                ],
                'informationProviderOfTradeItem' => [
                    'gln' => '3',
                ],
                'tradeItemFoo' => 'tradeItem bar',
                'gtin' => '1',
                'ean' => '42',
            ]
        ];

        $this->setExpectedException('Exception');
        $this->dal->catalogueItemSave($catalogueItem, 'SA24_test');
    }
}
