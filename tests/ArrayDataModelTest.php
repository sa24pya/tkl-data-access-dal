<?php
/**
 * File: tests/ArrayDataModelTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package DataAccessDAL
 * @subpackage Filter
 * @version 1.0.4
 *
 */

namespace DataAccessDAL\Filter;

/**
 * Class DALTest
 *
 * @package DataAccessDAL
 * @subpackage Filter
 * @version 1.0.0
 */
class  ArrayDataModelTest extends \PHPUnit_Framework_TestCase
{
    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";

    public function setUp() {}
    public function tearDown() {}


    /**
     * Test collection exists
     */
    public function testCanConstruct(){
      $o = new ArrayDataModel();
      /** @todo check the class name is proper */
      $this->assertTrue(!empty($o));
    }
}
