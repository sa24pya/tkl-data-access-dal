<?php
/**
 * File: FlattenCatalogue.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.0
 */

namespace DataAccessDAL\Transformer;

/**
 * Class FlattenCatalogue
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.0
 */
class FlattenCatalogue 
{

    /**
     * 
     * @var array
     */

    private $catalogueItemsFlat = [];

    /**
     * 
     * @return
     */
    private function get_catalogue_item( $catalogue ){

        // each catalogue or child item link MUST have one and only one catalogueItem
    
        if( !isset(  $catalogue->catalogueItem ) ){
            throw new \Exception("catalogueItem not set in: " . print_r($catalogue, true) );
        }
        if( !$catalogue instanceof \stdClass ) {
            throw new \Exception("catalogue not an instance of MongoDB\Model\BSONDocument. Instead instance of " . get_class($catalogue) );
        }
    
        return $catalogue->catalogueItem;
    }  



    /**
     * It makes a flat vector of CatalogueItems, from the one that is passed 
     * as argument, flattering the GS1 hierarchy into a vetor of "exploded" CatalogueItems.
     * 
     * @return array
     */
    public function flatten( \stdClass $catalogue, int $depth = 0 ){

        if( $depth > 3 ){
            throw new \Exception("recursion level of $depth exceeds maximum allowed");
        }

    
        $catalogueItem = $this->get_catalogue_item( $catalogue ); 
    
        // each catalogue item CAN have 1 or more child items (or none)
    
        if( isset(  $catalogueItem->catalogueItemChildItemLink) ){
        
            $childItemLink = $catalogueItem->catalogueItemChildItemLink;

            if( $childItemLink instanceof \stdClass ) {
                // catalogue item has only one child: add its cooresponding item to the array

                $this->flatten( $childItemLink, $depth + 1 );
            
                array_push( $this->catalogueItemsFlat, $this->get_catalogue_item( $childItemLink ) );
            }
            elseif( is_array( $childItemLink ) ) {
                // catalogue item has multiple childs: each one with it's own catalogue item
            
                $count = count( $catalogueItem->catalogueItemChildItemLink );

                foreach( $catalogueItem->catalogueItemChildItemLink as $subItem ){

                    $this->flatten( $subItem, $depth + 1 );
                    
                    array_push( $this->catalogueItemsFlat, $this->get_catalogue_item( $subItem ) );
                                }            
            }
            else{
                throw new \Exception("Child Item Link of unknown class " . get_class($childItemLink) );
            }
         
            // flatten the data structure: remove the childs before adding it to the catalogue items array
            unset(  $catalogueItem->catalogueItemChildItemLink);
        }

        array_push( $this->catalogueItemsFlat, $catalogueItem );
    }

    /**
     *
     * @return 
     */
    function get_catalogueItems(){
        return $this->catalogueItemsFlat;
    }
    
    /**
     *
     * @return 
     */
    function get_size(){
        return count( $this->catalogueItemsFlat );
    }
}
