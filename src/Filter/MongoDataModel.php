<?php 
/**
 * File: MongoDataModel.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.4
 *
 */
namespace DataAccessDAL\Filter;

/**
 * Filter for Mongo Data Model
 *
 * DAL expects to recive filters like 
 *     'tradeItem.gtin' => '87319470003671' or
 *     'tradeItem.informationProviderOfTradeItem.gln' => '5790000000302'
 *
 * however in mongo the collections are stored hierarchicaly, with up to
 * four levels of depth. Each level may contain several child items, each
 * of them with a catalogue item of it's own. Therefore, the queries 
 * have to be expanded before being sent to the mongodb like this:
 *
 *   child = "catalogueItemChildItemLink.catalogueItem"
 *   db.catalogue.findOne({ $or: [
 *     {"catalogueItem.tradeItem.gtin":gtin},
 *     {"catalogueItem.$child.tradeItem.gtin":gtin},
 *     {"catalogueItem.$child.$child.tradeItem.gtin":gtin},
 *     {"catalogueItem.$child.$child.$child.tradeItem.gtin":gtin},
 *   ]} )
 *
 *
 * @author Francesc Roma Frigole cesc2010@cesc.cat
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.4
 *
 */
class MongoDataModel 
{
    public static function expand(Array $in)
    {
        $out = [];

        self::pending( $in, $out );
        self::gtin( $in, $out );
        self::gln( $in, $out );

        return $out;
    }

    /* private static function add_filter
     *  
     * depending on how many filters we have, the mongo query will have
     * a different structure
     * if we have one filter, the structure will be 
     *           $or => $array_of_ored_queries
     * (remember that one filter becomes for ored queries one for each
     *  level of the catalogue hierarchy)
     *
     * but, if we have more than one filter, we need to $and them, so we
     * will query for example a catalogue that has one of the items which
     * matches a given GTIN and has one of the items which matches a 
     * given GLN, and will take this strucure:
     *    $and => [ [0] => [ gtin $or ], [1] => [ gln $or]  
     * note that we will get a collection result even if no item matches
     * both filters but differnt items match them separately.
     * the class ArrayDataModel will fix it.
     *
     */
    private static function add_filter(Array $or_filter, &$filters ){
        // if it's the first filter, just make the whole filter an $or

        if( count( $filters ) === 0 ){
            $filters['$or'] = $or_filter;
            return;
        }

        // if it's the second filter, make an and of the provious $or
        // and the new $or

        if( count( $filters ) === 1 ){
            $and_filter = [ '$and' => [] ];
            array_push( $and_filter['$and'], $filters );
            $filters = $and_filter;
            return;
        }

        // otherwise the structure already exists, we just need to
        // push the new value to the existing filters

        array_push( $filters['$and'], [ '$or' => $or_filter ] );
                
    }
    private static function pending( Array $in, Array &$out ){
        if( isset( $in['push.pending'] ) ) {
            $pending = $in['push.pending'];

            $filter = [
                ["push.pending" => $pending ]
            ];

            self::add_filter( $filter, $out );

        }        
    }
    private static function gtin( Array $in, Array &$out ){
        if( isset( $in['tradeItem.gtin'] ) ) {
            $gtin = $in['tradeItem.gtin'];

            $child = "catalogueItemChildItemLink.catalogueItem";

            $filter = [
                ["catalogueItem.tradeItem.gtin" => "$gtin" ],
                ["catalogueItem.$child.tradeItem.gtin" => "$gtin" ],
                ["catalogueItem.$child.$child.tradeItem.gtin" => "$gtin"],
                ["catalogueItem.$child.$child.$child.tradeItem.gtin" => "$gtin" ]
            ];

            self::add_filter( $filter, $out );
        }    
    }


    private static function gln( Array $in, Array &$out ){
        if( isset( $in['tradeItem.informationProviderOfTradeItem.gln'] ) ) {
            $gln = $in['tradeItem.informationProviderOfTradeItem.gln'];
            
            $child = "catalogueItemChildItemLink.catalogueItem";

            $filter = 
[
  ["catalogueItem.tradeItem.informationProviderOfTradeItem.gln" => "$gln" ],
  ["catalogueItem.$child.tradeItem.informationProviderOfTradeItem.gln" => "$gln"],
  ["catalogueItem.$child.$child.tradeItem.informationProviderOfTradeItem.gln" => "$gln" ],
  ["catalogueItem.$child.$child.$child.tradeItem.informationProviderOfTradeItem.gln" => "$gln" ] 
];

            self::add_filter( $filter, $out );
        }
    }   
}
