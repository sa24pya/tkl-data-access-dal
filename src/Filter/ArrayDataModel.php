<?php 
/**
 * File: ArrayDataModel.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.4
 *
 */

namespace DataAccessDAL\Filter;


/**
 * Filter for Array Data Model
 *
 * Each document in mongo is a catalogue of trade items.
 * Each catalogue can have a lot of trade items. When we query the database
 * if we find a trade item in the collection that matches the query we return
 * the whole collection, with all the items that don't match the query coming
 * along as well
 *
 * this class responsibility is to filter out those trade items which don't
 * match the requested filter
 *
 * Precondition: mongodb results have to be flattened with FlattenCatalogue
 * before being passed to this class.
 *
 *
 * DAL expects to recive filters like
 *     'tradeItem.gtin' => '87319470003671' or
 *     'tradeItem.informationProviderOfTradeItem.gln' => '5790000000302'
 *
 * @author Francesc Roma Frigole cesc@roma.cat
 * 
 * @see FlattenCatalogue, MongoDataModel
 * @package DataAccessDAL
 * @subpackage DAL
 * @version 1.0.4
 * 
 */
class ArrayDataModel 
{

    private static function filterElement( Array $filters, \stdClass $element ){
        $match = true;

        foreach( $filters as $longKey => $value ){

            // change filter string dots for object acces operations
            // example:
            //    tradeItem.informationProviderOfTradeItem.gln
            // would become
            //    tradeItem->informationProviderOfTradeItem->gln

            $keys = explode( '.', $longKey );
            $property = $element;
            foreach( $keys as $key ) {
                $property = $property->$key;
            }

            if( $property !== $value ){
                return false;
            }
        }

        return $match;
    }

    public static function filter( Array $filters, Array &$flatCollection ){

        foreach( $flatCollection as $key => $element ){
            if( !self::filterElement( $filters, $element ) ) {
                unset( $flatCollection[$key] );
            }
        }
    }
}
