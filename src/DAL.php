<?php
/**
 * File: DAL.php
 *
 * @author Francesc Roma Frigole cesc@roma.cat, mrx, victor
 * @version 1.0
 * @package application
 */

namespace DataAccessDAL;

use DataAccessDAL\Transformer\FlattenCatalogue;

use DataAccessDAL\Filter\MongoDataModel as MongoDataModel;
use DataAccessDAL\Filter\ArrayDataModel as ArrayDataModel;

/**
 * Class DAL
 *
 *
 * @package application
 * @subpackage DataAccessDAL
 */
class DAL
{
    /**
     * Token char length.
     * SECURITY enforce long token.
     * @var unknown
     */
    const TOKEN_LENGTH = 78;

    /**
     * Token expiration.
     * Number seconds a session will last
     * @var unknown
     */
    const TOKEN_SESSION_EXPIRE_SECONDS = 600;

    /**
     * max number of document for query
     * @var integer
     */
    const MONGO_MAX_DOCUMENTS = 1000;

    /**
     * @var object DAL
     */
    private static $instance;

    /**
     * server IP/DNS
     * @var string
     */
    const MONGO_SERVER = 'localhost';

    /**
     * Server port
     * @var string
     */
    const MONGO_PORT = '27017';

    /**
     *
     * @var unknown
     */
    const MONGO_DB_NAME = 'SA24';

    /**
     * MongoDB\Driver\Manager
     * @var object
     */
    private static $manager;

    /**
     * Mongo ReadPreference (Route to primary member of a replica set)
     * RP_PRIMARY is default
     *
     * @var object MongoDB\Driver\ReadPreference
     */
    private static $readPreference;

    /**
     * Write concern
     * @var object MongoDB\Driver\WriteConcern
     */
    private static $writeConcern;

    /**
     * MongoDB\Driver\BulkWrite
     * @var object
     */
    private static $bulkWrite;

    /**
     * The GS1 catalogue collection name.
     * @var string
     */
    protected static $mongoCollectionName = 'GS1_catalogue';

    /**
     * The SA24 catalogue collection name.
     * @var string
     */
    protected static $mongoCollectionSA24Name = 'SA24_catalogue';

    /**
     * Token data
     * @var array
     */
    private $tokenData;

    /**
     * last generated token
     * @var array
     */
    public $token;

    /**
     * Collection of errors that are generated during bunk upsert operations.
     * @var array
     */
    public $bulkErrors;


    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return object
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new DAL();
            static::$instance::$manager = new \MongoDB\Driver\Manager(
                "mongodb://" . self::MONGO_SERVER . ':' . self::MONGO_PORT
            );

            // Mongo ReadPreference (Route to primary member of a replica set)
            // RP_PRIMARY is default
            static::$instance::$readPreference = new \MongoDB\Driver\ReadPreference(
                \MongoDB\Driver\ReadPreference::RP_PRIMARY
            );

            // Write concern
            static::$instance::$writeConcern = new \MongoDB\Driver\WriteConcern(
                \MongoDB\Driver\WriteConcern::MAJORITY,
                1000
            );

            // Bulk Write
            static::$instance::$bulkWrite = new \MongoDB\Driver\BulkWrite;
        }
        return static::$instance;
    }

    /**
     * setCollections() : Overrides the default collections name, used for testing.
     * @return type
     */
    public static function setCollections($mongoCollectionName, $mongoCollectionSA24Name)
    {
        static::$mongoCollectionName = $mongoCollectionName;
        static::$mongoCollectionSA24Name = $mongoCollectionSA24Name;
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

    public function getManager()
    {
        return static::$instance::$manager;
    }

    public function getCollectionNames()
    {
        $mongoDBName = self::MONGO_DB_NAME;
        $getColNames = ["listCollections" => []];
        $cursor = static::$instance::$manager->executeCommand($mongoDBName, new \MongoDB\Driver\Command($getColNames));
        return $cursor->toArray();
    }

    public function createCollection($name)
    {
        $mongoDBName = self::MONGO_DB_NAME;
        $getColNames = ["create" => "$name"];
        $cursor = static::$instance::$manager->executeCommand($mongoDBName, new \MongoDB\Driver\Command($getColNames));
        return $cursor->toArray();
    }

    public function dropCollection($name)
    {
        $mongoDBName = self::MONGO_DB_NAME;
        $getColNames = ["drop" => "$name"];
        $cursor = static::$instance::$manager->executeCommand($mongoDBName, new \MongoDB\Driver\Command($getColNames));
        return $cursor->toArray();
    }


    /**
     * Read in person collection
     * @param array $args
     * @throws Exception
     * @todo select columns (fields)
     */
    public function personRead(array $args)
    {
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'person';

        // Query Filters
        $filters = $args['filters'];
        if (!is_array($filters)) {
            $filters = json_decode($filters);
        }

        // Query Options
        $options = array(
            "sort" => array(
                "_id" => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS
        );
        // Query
        $query = new \MongoDB\Driver\Query($filters, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );
        $aPersons = iterator_to_array($cursor);
        return $aPersons;
    }

    /**
     * Save in person collection
     * email & client are required
     *
     * @param array $args : the person doc
     * @return object MongoDB\Driver\WriteResult
     */
    public function personSave(\stdClass $person)
    {
        $person = (array)$person;
        // Validate inputs
        $key = 'email';
        if (!array_key_exists($key, $person)) {
            $messge = "Missing argument: $key";
            throw new \Exception($messge);
        }
        $key = 'client';
        if (!array_key_exists($key, $person)) {
            $messge = "Missing argument: $key";
            throw new \Exception($messge);
        }

        // Clean data
        $key = 'token';
        if (array_key_exists($key, $person)) {
            unset($person[$key]);
        }

        // Operation: upsert unique user by email-client fileds
        $sanitize = ['email' => null, /* 'password' => null, */
            'client' => null];
        $uniques = array_replace($sanitize, $person);
        static::$instance::$bulkWrite->update(
            $uniques,
            ['$set' => $person],
            ['multi' => false, 'upsert' => true]
        );

        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'person';

        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName.$mongoCollectionName",
            static::$instance::$bulkWrite
        );
        return $result;
    }

    public function tokenCreate(\stdClass $person)
    {
        $token = $this->generateNewTokenData();
        $token["person_id"] = $person->_id;
        $result = $this->tokenSave((object) $token);
        if (1 == $result->getUpsertedCount() || 1 == $result->getInsertedCount()) {
            return (object) $token;
        } else {
            throw new \Exception("Token create error");
        }
    }

    public function tokenSave(\stdClass $token)
    {
        $bulk = new \MongoDB\Driver\BulkWrite;
        $filters = [];
        if(isset($token->code)){
            $filters["code"] = $token->code;
        }
        $bulk->update(
            $filters,
            $token,
            ['multi' => false, 'upsert' => true]
        );
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'token';
        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName.$mongoCollectionName",
            $bulk
        );
        return $result;
    }

    public function tokenRead(array $args)
    {
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'token';

        // Query Filters
        $filters = $args['filters'];
        if (!is_array($filters)) {
            $filters = (array)json_decode($filters);
        }
        unset($filters["expire"]);
        unset($filters["person_id"]);

        // Query Options
        $options = array(
            "sort" => array(
                "_id" => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS
        );
        // Query
        $query = new \MongoDB\Driver\Query($filters, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );
        $tokens = iterator_to_array($cursor);
        if (isset($tokens[0])) {
            return $tokens[0];
        }else{
            return null;
        }
    }

    public function tokenDelete(array $args)
    {

        // Query Filters
        $filters = $args['filters'];
        if (!is_array($filters)) {
            $filters = (array)json_decode($filters);
        }
        //$filters = json_decode($filters);
        unset($filters["expire"]);
        unset($filters["person_id"]);

        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->delete(
            $filters
        );

        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'token';
        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName.$mongoCollectionName",
            $bulk
        );
        return $result;
    }


    /**
     * person Token Create
     * Create a token for a person in filters ($person)
     *
     * @param \stdClass $person
     * @return object MongoDB\Driver\WriteResult
     */
    public function personTokenCreate(\stdClass $person)
    {
        $person = $person;
        // Operation: upsert
        $token = $this->generateNewTokenData();
        $_id = array();
        if (isset($person->_id)) {
            $_id = ["_id" => $person->_id];
        }
        static::$instance::$bulkWrite->update(
            $_id,
            ['$set' => ['token' => $token]],
            ['multi' => false, 'upsert' => true]
        );
        $this->token = (object)$token;

        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'person';
        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName.$mongoCollectionName",
            static::$instance::$bulkWrite
        );
        return $result;
    }


    /**
     * person Token Create
     * Create a token for a person in filters ($person)
     *
     * @param \stdClass $person
     * @return object MongoDB\Driver\WriteResult
     */
    public function personTokenDelete(array $filters)
    {
        $person = (object)$filters;
        // Operation: upsert
        $token = new \stdClass();
        $_id = array();
        if (isset($person->_id)) {
            $_id = ["_id" => $person->_id];
        }
        static::$instance::$bulkWrite->update(
            $_id,
            ['$set' => ['token' => $token]],
            ['multi' => false, 'upsert' => true]
        );
        $this->token = (object)$token;

        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Collection name
        $mongoCollectionName = 'person';
        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName.$mongoCollectionName",
            static::$instance::$bulkWrite
        );
        return $result;
    }

    public function pendingImageRead()
    {
        $mongoDBName = self::MONGO_DB_NAME;
        // Query Filters
        $filtersInput = array(
            "image.pending" => array(
                '$eq' => true,
            ),
        );
        // Projection:
        $link = "catalogueItemChildItemLink";
        $child = "$link.catalogueItem";
        $gtin = "tradeItem.gtin";
        $gln = "tradeItem.informationProviderOfTradeItem.gln";
        $ref = "referenced_file_detail_information:referencedFileDetailInformationModule";
        $innerChild = "tradeItem.tradeItemInformation.extension.$ref";
        // The array:
        $projection = array(
            '_id' => 0,
            // Load the gtin recursively:
            "catalogueItem.$gtin" => 1,
            "catalogueItem.$child.$gtin" => 1,
            "catalogueItem.$child.$child.$gtin" => 1,
            "catalogueItem.$child.$child.$child.$gtin" => 1,
            // Load the gln since the sa24 needs it:
            "catalogueItem.$gln" => 1,
            "catalogueItem.$child.$gln" => 1,
            "catalogueItem.$child.$child.$gln" => 1,
            "catalogueItem.$child.$child.$child.$gln" => 1,
            // The actual image data:
            "catalogueItem.$innerChild" => 1,
            "catalogueItem.$child.$innerChild" => 1,
            "catalogueItem.$child.$child.$innerChild" => 1,
            "catalogueItem.$child.$child.$child.$innerChild" => 1,
            // Knowing the number of childs it has is important to detect if its an array or not:
            "catalogueItem.$link.quantity" => 1,
            "catalogueItem.$child.$link.quantity" => 1,
            "catalogueItem.$child.$child.$link.quantity" => 1,
            "catalogueItem.$child.$child.$child.$link.quantity" => 1,
        );
        $options = array(
            "sort" => array(
                "_id" => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS
        );
        $options['projection'] = $projection;
        // Query for GS1 collection (data from providers)
        $query = new \MongoDB\Driver\Query($filtersInput, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName." . static::$instance::$mongoCollectionName,
            $query,
            static::$instance::$readPreference
        );
        $dataArray = [];
        foreach ($cursor as $catalogue) {
            $dataArray[] = $catalogue;
        }
        return $dataArray;
    }

    /**
     * READ catalogueItems
     *
     * @param array $args
     */
    public function catalogueItemRead(array $args)
    {
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;

        // Query Filters
        $filtersInput = $args['filters'];
        if (array_key_exists('fields', $args)) {
            $fields = array('_id' => 0);
            foreach ($args['fields'] as $field) {
                $fields[$field] = 1;
            }
        }
        $options = array(
            "sort" => array(
                "_id" => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS
        );
        if (isset($fields) && count($fields)) {
            $options['projection'] = $fields;
        }        // Query Options

        // convert filters to mongoDB
        $filtersMongo = MongoDataModel::expand($filtersInput);

        // Query for GS1 collection (data from providers)
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName." . static::$instance::$mongoCollectionName,
            $query,
            static::$instance::$readPreference
        );

        // var_dump($cursor);


        $flatten = new FlattenCatalogue();
        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }

        $GS1FlatCollection = $flatten->get_catalogueItems();

        ArrayDataModel::filter($filtersInput, $GS1FlatCollection);

        // same for SA24 db (data from manager)
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName." . static::$instance::$mongoCollectionSA24Name,
            $query,
            static::$instance::$readPreference
        );

        $flatten = new FlattenCatalogue();
        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }

        $SA24FlatCollection = $flatten->get_catalogueItems();

        ArrayDataModel::filter($filtersInput, $SA24FlatCollection);

        //file_put_contents( "result.txt" , var_export($GS1FlatCollection, true));
        //file_put_contents( "result.txt" , var_export($GS1FlatCollection, true));

        return [
            'SA24' => $SA24FlatCollection,
            'GS1' => $GS1FlatCollection
        ];
    }

    /**
     * gs1ItemSaveBulk(): GS1 catalogue item write in bulk mode for gs1 import.
     * @param array $documents
     * @return type
     */
    public function gs1ItemSaveBulk(array $documents)
    {
        $bulkGS1 = new \MongoDB\Driver\BulkWrite;

        // Write concern
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        foreach ($documents as $document) {
            $gtin = $document['catalogueItem']['tradeItem']['gtin'];
            if (empty($gtin)) {
                $message = "tradeItem-gtin is empty in: \n"
                    . var_export($document, true);
                throw new \Exception($message);
            }
            $bulkGS1->update(
                ['catalogueItem.tradeItem.gtin' => $gtin],
                ['$set' => $document],
                ['multi' => false, 'upsert' => true]
            );
        }
        $result = static::$instance::$manager->executeBulkWrite(self::MONGO_DB_NAME . "." . static::$instance::$mongoCollectionName, $bulkGS1, $writeConcern);
        return $result;
    }

    public function setGS1ImagePendingFalse(array $gtins)
    {
        $bulkGS1 = new \MongoDB\Driver\BulkWrite;
        // Write concern
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        foreach ($gtins as $gtin) {
            $bulkGS1->update(
                ['catalogueItem.tradeItem.gtin' => $gtin],
                ['$set' => [
                    'image' => [
                        'pending' => false,
                        'timestamp' => time(),
                    ],
                ]],
                ['multi' => false, 'upsert' => true]
            );
        }
        $result = static::$instance::$manager->executeBulkWrite(self::MONGO_DB_NAME . "." . static::$instance::$mongoCollectionName, $bulkGS1, $writeConcern);
        return $result;
    }
    public function setSA24PushPendingFalse(array $gtins)
    {
        $bulkSA24 = new \MongoDB\Driver\BulkWrite;
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        foreach ($gtins as $gtin) {
            $bulkSA24->update(
                ['catalogueItem.tradeItem.gtin' => $gtin],
                ['$set' => [
                    'push' => [
                        'pending' => false,
                        'timestamp' => time(),
                    ],
                ]],
                ['multi' => false, 'upsert' => true]
            );
        }
        if (!empty($gtins)) {
            $result = static::$instance::$manager->executeBulkWrite(self::MONGO_DB_NAME . "." . static::$instance::$mongoCollectionSA24Name, $bulkSA24, $writeConcern);
            return $result;
        }
        return false;
    }

    /**
     * Create or Update catalogueItem
     *
     * This method only operates in the SA24 collection
     * the GS1 collection is written in bulk by the importer instead
     *
     * @param array $catalogueItem
     */
    public function catalogueItemSave(\stdClass $document)
    {
        return $this->catalogueItemSaveBulk([$document]);
    }

    /**
     * Catalogue Item write in bulk mode for no-GS1 import from API.
     * @param array $documents
     */
    public function catalogueItemSaveBulk(array $documents)
    {
        // Validation of insert/update constraints
        $errorsValidation = $this->validateCatalogueItems($documents);
        if (!empty($errorsValidation)) {
            $documents = array_diff_key($errorsValidation, $documents);
            // Error collection
            $this->bulkErrors = $errorsValidation;
        }
        $bulkWrite = new \MongoDB\Driver\BulkWrite;
        // Cycle on $documents
        foreach ($documents as $i => $document) {
            // Better as array
            $document = (array)$document;
            $document = ['catalogueItem' => $document];
            $document['insertTimestamp'] = time();
            $document['source'] = 'API';
            $document = $this->cleanUpEmptyValues($document);
            // Uniques
            $uniques = $this->generateUniquesIfExist($document);
            // Update
            $bulkWrite->update(
                $uniques,
                [
                    '$set' => $document
                ],
                [
                    'multi' => false,
                    'upsert' => true
                ]
            );
        }
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;
        // Write concern
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );
        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName." . static::$instance::$mongoCollectionSA24Name,
            $bulkWrite,
            $writeConcern
        );
        return $result;
    }

    /**
     *  delete one catalogue item
     *
     *  removes the item with a given tradeItem.id from both collections
     *  GS1 and SA24
     *
     * @param string $gtin
     */

    public function catalogueItemDelete($gtin)
    {
        $filterBasic = ['tradeItem.gtin' => "$gtin"];
        $filterSA24 = ['catalogueItem.tradeItem.gtin' => "$gtin"];
        $filterGS1 = MongoDataModel::expand($filterBasic);

        // DB name
        $mongoDBName = self::MONGO_DB_NAME;

        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->delete(
            $filterGS1,
            ['limit' => 1]
        );

        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName." . static::$instance::$mongoCollectionName,
            $bulk
        );

        // executeBulkWrite
        $result = static::$instance::$manager->executeBulkWrite(
            "$mongoDBName." . static::$instance::$mongoCollectionSA24Name,
            $bulk
        );
    }

    /**
     * cleanUpEmptyValues(): Clean up the unique keys if they're found with a falsy value.
     * @param mixed $document : The document to check
     * @return array
     */
    public function cleanUpEmptyValues($document)
    {
        $document = json_decode(json_encode($document), true);
        if (isset($document['catalogueItem']['tradeItem']['gtin']) &&
            $document['catalogueItem']['tradeItem']['gtin'] == false
        ) {
            unset($document['catalogueItem']['tradeItem']['gtin']);
        }
        $value = '@value';
        if (isset($document['catalogueItem']['tradeItem']['ean']) &&
            $document['catalogueItem']['tradeItem']['ean'] == false
        ) {
            unset($document['catalogueItem']['tradeItem']['ean']);
        }
        $value = '@value';
        if (isset($document['catalogueItem']['tradeItem']['additionalTradeItemIdentification'][$value]) &&
            $document['catalogueItem']['tradeItem']['additionalTradeItemIdentification'][$value] == false
        ) {
            unset($document['catalogueItem']['tradeItem']['additionalTradeItemIdentification'][$value]);
        }
        return $document;
    }

    /**
     * generateUniquesIfExist(): Expects a SA24 document.
     * @param array $document : The document to check
     * @return array
     */
    public function generateUniquesIfExist($document)
    {
        $uniques = [];
        if (isset($document['catalogueItem']['tradeItem']['gtin'])) {
            $uniques['catalogueItem.tradeItem.gtin'] = $document['catalogueItem']['tradeItem']['gtin'];
        }
        $value = '@value';
        if (isset($document['catalogueItem']['tradeItem']['ean'])) {
            $uniques['catalogueItem.tradeItem.ean'] = $document['catalogueItem']['tradeItem']['ean'];
        }
        if (isset($document['catalogueItem']['tradeItem']['additionalTradeItemIdentification'][$value])) {
            $uniques['catalogueItem.tradeItem.additionalTradeItemIdentification.@value'] = $document['catalogueItem']
            ['tradeItem']['additionalTradeItemIdentification'][$value];
        }
        return $uniques;
    }


    /**
     * Generate new token data
     * @return array
     */
    private function generateNewTokenData()
    {
        $code = bin2hex(random_bytes(self::TOKEN_LENGTH));
        $dt = new \DateTime('NOW', new \DateTimeZone('Europe/Copenhagen'));
        $dt->add(new \DateInterval('PT300M'));
        $expire = $dt->format('c');
        $token = ['code' => $code, 'expire' => $expire];
        return $token;
    }


    /**
     * Validate a document of type SA24_catalogue (CatalogueItem)
     * before insertion.
     *
     * @param \stdClass $document
     * @throws Exception
     */
    private function validateCatalogueItem(\stdClass $document)
    {
        // convert to array recursively
        $document = json_decode(json_encode($document), true);
        if (array_key_exists('_id', $document)) {
            // Update

        } else {
            // Insert
            // Supplier Identification
            // GLN|CVRNumber MUST be set

            $gln = (isset($document['tradeItem']['informationProviderOfTradeItem']['gln']))
                ? $document['tradeItem']['informationProviderOfTradeItem']['gln'] : null;
            $cvrNumber = (isset($document['tradeItem']['informationProviderOfTradeItem']['vat']['cvrNumber']))
                ? $document['tradeItem']['informationProviderOfTradeItem']['vat']['cvrNumber'] : null;
            if (empty($gln) && empty($cvrNumber)) {
                $message = "CatalogueItem must have a GLN|CVRNumber"
                    . " in order to be inserted. GLN: '$gln', CVR n.: '$cvrNumber', Document: "
                    . var_export($document, true);
                // Throws Exception
                throw new \Exception($message, 400);
            }

            // CataloguieItem (Product) Identification in 3rd systems
            // GTIN|EAN|articleNumber MUST be set
            $gtin = (isset($document['tradeItem']['gtin']))
                ? $document['tradeItem']['gtin'] : null;
            $ean = (isset($document['tradeItem']['ean']))
                ? $document['tradeItem']['ean'] : null;
            $articleNumber = (isset($document['tradeItem']['additionalTradeItemIdentification']['@value']))
                ? $document['tradeItem']['additionalTradeItemIdentification']['@value'] : null;
            $additionalIdentificationTypeCode = (isset($document['tradeItem']['additionalTradeItemIdentification']['@attributes']['additionalIdentificationTypeCode']))
                ? $document['tradeItem']['additionalTradeItemIdentification']['@attributes']['additionalIdentificationTypeCode'] : null;
            if (empty($gtin) && empty($ean)
                && ((empty($articleNumber)
                    || (!empty($articleNumber)
                        && 'SUPPLIER_ASSIGNED' != $additionalIdentificationTypeCode)))
            ) {
                $message = "CatalogueItem must have a GTIN|EAN|articleNumber"
                    . " in order to be inserted. Document: "
                    . var_export($document, true);
                // Throws Exception
                throw new \Exception($message, 400);
            }
        }
    }


    /**
     * Validate a vector of documents of type SA24_catalogue (CatalogueItem)
     * before insertion.
     * @param array $documents
     * @return array An array of errors (if any)
     */
    private function validateCatalogueItems(array $documents)
    {
        $validationMessages = array();
        foreach ($documents as $i => $document) {
            try {
                $this->validateCatalogueItem($document);
            } catch (\Exception $e) {
                $validationMessages[$i] = $e->getMessage();
            }
        }
        return $validationMessages;
    }

    private function buildFilterCatalogueItem($document)
    {
        // Filters init
        $filters = array();

        // Supplier Identification
        // GTIN|EAN|articleNumber
        $gtin = null;
        $ean = null;
        $articleNumber = null;

        if (!empty($document{'tradeItem'}{'gtin'})) {
            $gtin = $document{'tradeItem'}{'gtin'};
            $filters = array_merge($filters, array('tradeItem.gtin' => $gtin));
        } elseif (!empty($document{'tradeItem'}{'ean'})) {
            $ean = $document{'tradeItem'}{'ean'};
            $filters = array_merge($filters, array('tradeItem.ean' => $ean));
        } elseif (!empty($document{'tradeItem'}{'additionalTradeItemIdentification'}{'@value'})
            && 'SUPPLIER_ASSIGNED' == $typeCode
        ) {
            $articleNumber = $document{'tradeItem'}{'additionalTradeItemIdentification'}{'articleNumber'}{'@value'};
            $filters = array_merge($filters, array('tradeItem.additionalTradeItemIdentification.articleNumber.@value' => $articleNumber));
        }


        return $filters;
    }


    private function buildFilterCatalogueItems($documents)
    {
        $filters = array();
        foreach ($documents as $i => $document) {
            $filters[$i] = $this->buildFilterCatalogueItem($document);
        }
        return $filters;
    }


    /**
     * This method finds all the catalogueItems in GSA24 that are marked for pushing to the shops (for module M4).
     * A specific function is needed because catalogueItemRead cannot filter for elements outside catalogueItem attribute
     * and the push status information is at the same level as catalogueItem, not inside it
     * @param any
     * @throws Exception
     */

    public function catalogueItemGetGtin4Push()
    {
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;

        // Query Filters

        $filtersInput = [
            'push.pending' => true
        ];

        $options = array(
            'sort' => array(
                '_id' => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS,
            'projection' => array(
                '_id' => 0,
                'catalogueItem.tradeItem.gtin' => '1'
            )
        );

        // convert filters to mongoDB
        $filtersMongo = MongoDataModel::expand($filtersInput);

        $mongoCollectionName = static::$instance::$mongoCollectionSA24Name;
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );

        $flatten = new FlattenCatalogue();
        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }
        $SA24FlatCollection = $flatten->get_catalogueItems();

        $mongoCollectionName = static::$instance::$mongoCollectionName;
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );

        $flatten = new FlattenCatalogue();
        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }
        $GS1FlatCollection = $flatten->get_catalogueItems();


        //Filter\ArrayDataModel::filter( $filtersInput, $SA24FlatCollection );

        return [
        'GS1' => $GS1FlatCollection,
        'SA24' => $SA24FlatCollection];

    }


    /*
     * this method retuns all catalogueItems in GS1 and SA24 catalogue that must be pushed,
     * 
     */
    public function catalogueItemRead4Push(array $args)
    {
        // DB name
        $mongoDBName = self::MONGO_DB_NAME;

        // Query Filters
        $filtersInput = $args['filters'];
        if (array_key_exists('fields', $args)) {
            $fields = array('_id' => 0);
            foreach ($args['fields'] as $field) {
                $fields[$field] = 1;
            }
        }
        $options = array(
            "sort" => array(
                "catalogueItem.tradeItem.gtin" => -1,
            ),
            'limit' => self::MONGO_MAX_DOCUMENTS
        );
        if (isset($fields) && count($fields)) {
            $options['projection'] = $fields;
        }
        // Query Options

        $filtersMongo = [
            'catalogueItem.tradeItem.gtin' => array('$in' => $args['filters'])
        ];

        // Query for GS1 collection (data from providers)
        $mongoCollectionName = static::$instance::$mongoCollectionName;
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);

        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );

        $flatten = new FlattenCatalogue();
        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }

        $GS1FlatCollection = $flatten->get_catalogueItems();

        // the same for SA24_catalogue

        $mongoCollectionName = static::$instance::$mongoCollectionSA24Name;
        $query = new \MongoDB\Driver\Query($filtersMongo, $options);


        // Query execution & Mongo Cursor
        $cursor = static::$instance::$manager->executeQuery(
            "$mongoDBName.$mongoCollectionName",
            $query,
            static::$instance::$readPreference
        );

        $flatten = new FlattenCatalogue();

        foreach ($cursor as $catalogue) {
            $flatten->flatten($catalogue);
        }

        $SA24FlatCollection = $flatten->get_catalogueItems();

        return [
            'SA24' => $SA24FlatCollection,
            'GS1' => $GS1FlatCollection
        ];

    }


}
